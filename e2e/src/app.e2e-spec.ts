import { browser, element, logging, by } from 'protractor';
import { protractor } from 'protractor/built/ptor';

/**
 * For these tests we assume that the backend is running,
 * and we work with the data we know to be present in the backend (i,e, user 1 who has 2 orders,
 * one of which has already been shipped)
 */
describe('empatica-website App', () => {

  beforeEach(async () => {
    await browser.get(browser.baseUrl + '/user/1/orders');
  });

  it('should show user info and a list of orders', async () => {
    expect(element(by.id('ordersText')).isPresent()).toBeTruthy();
    expect(element(by.className('user-orders__user-container')).isPresent()).toBeTruthy();

    const orderCards = element.all(by.className('order-card'));
    // For some reason I couldn't make this work without resorting to the 'then' construction, although that shouldn't be necessary
    orderCards.count().then((amount) => expect(amount).toEqual(2));
  });

  it('should collapse and expand an order', async () => {
    // When accessing the orders page, we expect all orders to be expanded and thus show the order content
    element(by.css('#order1 .order-card__order-content')).isPresent()
    .then((res) => expect(res).toBe(true));

    // When clicking the collapse button, we expect the order content to disappear
    await element(by.id('collapseOrderBtn1')).click();

    element(by.css('#order1 .order-card__order-content')).isPresent()
    .then((res) => expect(res).toBe(false));

    // When clicking the collapse button again, we expect the order content to reappear
    await element(by.id('collapseOrderBtn1')).click();

    element(by.css('#order1 .order-card__order-content')).isPresent()
    .then((res) => expect(res).toBe(true));
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});

