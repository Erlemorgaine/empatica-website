import { Discount, DiscountType } from '../models/order';
import { DiscountPipe } from './discount.pipe';

describe('DiscountPipe', () => {
  it('create an instance', () => {
    const pipe = new DiscountPipe();
    expect(pipe).toBeTruthy();
  });

  it('returns a percentage for a percent type discount', () => {
    const pipe = new DiscountPipe();
    expect(pipe.transform(25.5, DiscountType.percent)).toEqual('25.5%');
  });

  it('returns an amount in USD for an amount type discount', () => {
    const pipe = new DiscountPipe();
    expect(pipe.transform(575, DiscountType.amount)).toEqual('$575');
  });

  it('returns only the discount value for a discount without a (known) type', () => {
    const pipe = new DiscountPipe();
    expect(pipe.transform(23)).toEqual('23');
  });
});
