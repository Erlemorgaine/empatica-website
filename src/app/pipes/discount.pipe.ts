import { Pipe, PipeTransform } from '@angular/core';
import { DiscountType } from '../models/order';

@Pipe({
  name: 'discount'
})
export class DiscountPipe implements PipeTransform {

  transform(value: number, format?: DiscountType): string {
    switch(format) {
      case DiscountType.amount:
        return `$${value}`;
      case DiscountType.percent:
        return `${value}%`;
      default:
        return `${value}`;
    }
  }

}
