import { TestBed, tick, fakeAsync } from '@angular/core/testing';
import { of } from 'rxjs';
import { ActivatedRouteSnapshot } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AuthGuard } from './auth.guard';
import { RouterTestingModule } from '@angular/router/testing';
import { Location, LocationStrategy } from '@angular/common';
import { MockLocationStrategy } from '@angular/common/testing';
import { routes } from '../../app-routing.module';

describe('AuthGuardService', () => {
  let service: AuthGuard;
  let authenticationService: AuthenticationService;
  let location: Location;

  const route = new ActivatedRouteSnapshot();
  route.params = {userId: 1};

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        AuthenticationService,
        {provide: LocationStrategy, useClass: MockLocationStrategy}
       ]
    });
    service = TestBed.inject(AuthGuard);
    authenticationService = TestBed.inject(AuthenticationService);
    location = TestBed.inject(Location);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return true if authentication service returned true', () => {
    spyOn(authenticationService, 'isAuthenticated').and.returnValue(of(true));

    service.canActivate(route)
    .subscribe((res) => expect(res).toBeTrue());
  });

  it('navigate and return false if authentication service returned false', fakeAsync(() => {
    spyOn(authenticationService, 'isAuthenticated').and.returnValue(of(false));

    service.canActivate(route).subscribe((res) => expect(res).toBeFalse());
    tick();
    expect(location.path()).toBe('/');
  }));
});
