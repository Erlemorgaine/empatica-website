import { ToasterConfig } from 'angular2-toaster';

export const toasterConfig: ToasterConfig = new ToasterConfig({
    typeClasses: {
      error: 'toaster-error',
      success: 'toaster-success',
    },
    animation: 'slideUp',
    positionClass: 'toast-bottom-right'
});
