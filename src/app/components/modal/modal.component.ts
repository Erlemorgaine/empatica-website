import { Component, OnInit } from '@angular/core';
import { SimpleModalComponent } from 'ngx-simple-modal';
import { TranslateService } from '@ngx-translate/core';

export interface IModalData {
  translatePrefix: string;

}

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent extends SimpleModalComponent<IModalData, boolean> implements OnInit {
  translatePrefix = '';
  hasSecondButton = false;

  constructor(private translateService: TranslateService) {
    super();
  }

  ngOnInit(): void {
    this.checkForSecondBtn();
  }

  /**
   * Checks if there is a translation for the 'cancel' button, given the translatePrefix.
   * If not, the button won't be shown.
   */
  private checkForSecondBtn(): void {
    const cancelOptionKey = `${this.translatePrefix}.cancelOption`;
    this.hasSecondButton = this.translateService.instant(cancelOptionKey) !== cancelOptionKey;
  }

  confirm(): void {
    this.result = true;
    this.close();
  }

  cancel(): void {
    this.result = false;
    this.close();
  }
}
