import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../models/user';
import { Order, Orders } from '../models/order';

@Injectable({
  providedIn: 'root'
})
export class UserOrdersService {

  constructor(private http: HttpClient) { }

  getUser(userId: number): Observable<User> {
    return this.http.get<User>(`${environment.backendUrl}/users/${userId}`)
    .pipe(map((res) => new User().deserialize(res)));
  }

  getOrders(userId: number): Observable<Orders> {
    return this.http.get<Orders>(`${environment.backendUrl}/users/${userId}/orders`)
    .pipe(map((res) => new Orders().deserialize(res)));
  }

  deleteOrder(orderId: number): Observable<{ orderId: number; status: string; order: Order }> {
    return this.http.delete<{ orderId: number; status: string; order: Order }>(`${environment.backendUrl}/orders/${orderId}`)
    .pipe(map((res) => Object.assign(res, {order: new Order().deserialize(res.order)})));
  }
}
