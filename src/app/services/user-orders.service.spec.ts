import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { UserOrdersService } from './user-orders.service';

describe('UserOrdersService', () => {
  let service: UserOrdersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(UserOrdersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve user information', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve order information from the user', () => {
    expect(service).toBeTruthy();
  });
});
