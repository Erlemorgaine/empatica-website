import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  /**
   * Very simple authentication method that checks if the user id returned from the backend
   * is the same as the user id in the route.
   * If not, the user will not be permitted to navigate to that route.
   */
  isAuthenticated(userId: number): Observable<boolean> {
    return this.http.post<{id: number}>(`${environment.backendUrl}/login`, {})
    .pipe(map((result: {id: number}) => result?.id === userId));
  }
}
