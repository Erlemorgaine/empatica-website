import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AuthenticationService } from './authentication.service';
import { environment } from 'src/environments/environment';

describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.inject(AuthenticationService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return true if backend returns user id equal to user id provided', () => {
    const userId = 1;

    service.isAuthenticated(userId).subscribe((res) => expect(res).toBeTrue());
    const userReq = httpMock.expectOne(`${environment.backendUrl}/login`);
    userReq.flush({id: userId});

    httpMock.verify();
  });

  it('should return false if backend returns user id NOT equal to user id provided', () => {
    const userId = 1;
    const userIdFromBackend = 2;

    service.isAuthenticated(userId).subscribe((u) => expect(u).toBeFalse());
    const userReq = httpMock.expectOne(`${environment.backendUrl}/login`);
    userReq.flush({id: userIdFromBackend});

    httpMock.verify();
  });

  it('should return false if no user id came back from the backend', () => {
    service.isAuthenticated(1).subscribe((u) => expect(u).toBeFalse());
    const userReq = httpMock.expectOne(`${environment.backendUrl}/login`);
    userReq.flush(null);

    httpMock.verify();
  });
});
