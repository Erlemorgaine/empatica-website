import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SimpleModalService } from 'ngx-simple-modal';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ModalComponent } from 'src/app/components/modal/modal.component';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(public modalService: SimpleModalService) { }

  handleError(error: HttpErrorResponse) {
    if (error.status >= 500 || error.status === 0) {
      this.modalService.addModal(ModalComponent, {translatePrefix: 'unreachableServerModal'}).subscribe();
    }

    return throwError(error);
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(catchError((e) => this.handleError(e)));
  }
}
