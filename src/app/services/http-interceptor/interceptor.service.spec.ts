import { TestBed } from '@angular/core/testing';
import { SimpleModalModule, SimpleModalService } from 'ngx-simple-modal';

import { InterceptorService } from './interceptor.service';

describe('InterceptorService', () => {
  let service: InterceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ SimpleModalModule ],
      providers: [ SimpleModalService ]
    });
    service = TestBed.inject(InterceptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
