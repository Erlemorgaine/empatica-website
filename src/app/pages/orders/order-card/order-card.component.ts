import { Component, Input, OnInit } from '@angular/core';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { SimpleModalService } from 'ngx-simple-modal';
import { ModalComponent } from 'src/app/components/modal/modal.component';
import { Discount, DiscountType, Item, Order, ShippingStatus } from 'src/app/models/order';
import { toasterConfig } from 'src/app/components/toasterConfig';
import { UserOrdersService } from 'src/app/services/user-orders.service';
import { TranslateService } from '@ngx-translate/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

interface IOrderAmounts {
  subTotal: number;
  discount: number;
  shipping: number;
  total: number;
}

@Component({
  selector: 'app-order-card',
  templateUrl: './order-card.component.html',
  styleUrls: ['./order-card.component.scss']
})
export class OrderCardComponent implements OnInit {

  @Input() order!: Order;

  @Output() orderDeleted: EventEmitter<number> = new EventEmitter<number>();

  orderAmounts: IOrderAmounts = { subTotal: 0, discount: 0, shipping: 6.75, total: 0 };
  toasterConfig: ToasterConfig = toasterConfig;
  shippingStatus: typeof ShippingStatus = ShippingStatus;
  isCollapsed = false;

  constructor(
    private userOrderService: UserOrdersService,
    private simpleModalService: SimpleModalService,
    private toasterService: ToasterService,
    private translateService: TranslateService
  ) { }

  ngOnInit(): void {
    this.calculatePricing();
  }

    /**
     * Calculates the subtotal, total discount, and total price of an order,
     * taking into account the shipping costs.
     */
  calculatePricing(): void {
    const subTotal = this.order.items.reduce((a: number, b: Item) => a + b.amount, 0);
    const discount = this.order.discounts.reduce((a: number, b: Discount) => this.calculateDiscount(a, b, subTotal), 0);
    const total = subTotal - discount + this.orderAmounts.shipping;

    this.orderAmounts = Object.assign(this.orderAmounts, {subTotal, discount, total});
  }

  /**
   * Helper function to calculate the total discount.
   *
   * @param initialValue: discount calculated so far
   * @param discount: the discount to be added to discount calculated so far
   * @param subTotal: total price of all items in the order
   *
   */
  private calculateDiscount(initialValue: number, discount: Discount, subTotal: number): number {
    switch(discount.type) {
      case DiscountType.amount:
        return initialValue + discount.value;
      case DiscountType.percent:
        return initialValue + subTotal * (discount.value / 100);
      default:
        return initialValue;
    }
  }

  /**
   * Opens a modal to confirm cancellation of order.
   * On user confirmation, sends a request to delete order.
   * Shows a toast with the returned result.
   * If the order was successfully deleted, sent an event up so dat the parent component
   * can take care of showing the modified order list.
   */
  deleteOrder(orderId: number): void {
    this.simpleModalService.addModal(ModalComponent, { translatePrefix: 'userOrders.order.delete.modal' })
    .subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.userOrderService.deleteOrder(orderId).subscribe((res) => {
          const toasterStatus = res?.status === 'cancelled' ? 'success' : 'error';
          const toasterMessage = this.translateService.instant('userOrders.order.delete.toaster.' + toasterStatus);
          this.toasterService.pop(toasterStatus, '', toasterMessage);

          if (toasterStatus === 'success') {
            this.orderDeleted.emit(res.orderId);
          }
        });
      }
    });
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
  }

  /**
   * Helper function to override default (alphabetical) sorting of keyvalue pipe.
   */
  returnZero(): number {
    return 0;
  }
}
