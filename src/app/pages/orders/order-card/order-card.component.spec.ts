import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateLoader, TranslateModule, TranslatePipe, TranslateService } from '@ngx-translate/core';
import { createHttpLoaderFactory } from 'src/app/app.module';
import { TableComponent } from 'src/app/components/table/table.component';
import { Discount, DiscountType, Item, Order, OrderStatus } from 'src/app/models/order';
import { SimpleModalModule, SimpleModalService } from 'ngx-simple-modal';

import { OrderCardComponent } from './order-card.component';
import { ToasterModule, ToasterService } from 'angular2-toaster';

describe('OrderCardComponent', () => {
  let component: OrderCardComponent;
  let fixture: ComponentFixture<OrderCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        SimpleModalModule,
        ToasterModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: createHttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [
        OrderCardComponent,
        TableComponent,
        TranslatePipe
      ],
      providers: [
        TranslateService,
        SimpleModalService,
        ToasterService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderCardComponent);
    component = fixture.componentInstance;
    const order = Object.assign(new Order(), {id: 1, ref: 'dummyReference', status: 'dummyStatus'});
    component.order = order;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate the correct amounts for an order with percentage discount', () => {
    const itemValues = [100, 200];
    const discountValues = [10, 25];
    const subTotal = 300; // 100 + 200
    const discount = .35 * subTotal; // 10 + 25 = %35 discount

    testAmountCalculationHelper(itemValues, discountValues, DiscountType.percent, { subTotal, discount });
  });

  it('should calculate the correct amounts for an order with amount discount', () => {
    const itemValues = [249.50, 300.50];
    const discountValues = [100.25, 5.10];
    const subTotal = 550; // 249.50 + 300.50
    const discount = 105.35; // 100.25 + 5.10

    testAmountCalculationHelper(itemValues, discountValues, DiscountType.amount, { subTotal, discount });
  });

  it('should calculate the correct amounts for an order with mixed type discounts', () => {
    const itemValues = [50, 10.5];
    const discountValues = [10.5, 15];
    const subTotal = 60.5; // 50 + 10.75
    const discount = 21.3525; // 15 + (10.5% of 60.75, which is 6.3525)

    testAmountCalculationHelper(itemValues, discountValues, DiscountType.amount, { subTotal, discount }, true);
  });

  const testAmountCalculationHelper = (
    itemValues: number[],
    discountValues: number[],
    discountType: DiscountType,
    answerWithoutShipping: { subTotal: number; discount: number },
    mixedTypes: boolean = false
  ): void => {
    const items = itemValues.map((v, i) => Object.assign(new Item(), { sku: '', name: 'item' + i, amount: v }));
    const discounts = discountValues.map((v, i) => {
      let dType: DiscountType = discountType;

      if (mixedTypes) {
        dType = i % 2 === 1 ? DiscountType.amount : DiscountType.percent;
      }

      return Object.assign(new Discount(), { name: 'discount' + i, type: dType, value: v });
    });

    component.order = Object.assign(new Order(), {id: 1, ref: '', status: OrderStatus.paid, items, discounts});

    const expectedAmounts = Object.assign(answerWithoutShipping, {
      shipping: component.orderAmounts.shipping,
      total: answerWithoutShipping.subTotal - answerWithoutShipping.discount + component.orderAmounts.shipping
     });

    component.calculatePricing();

    expect(component.orderAmounts).toEqual(expectedAmounts);
  };
});
