import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Order, Orders } from 'src/app/models/order';
import { User } from 'src/app/models/user';
import { UserOrdersService } from 'src/app/services/user-orders.service';

@Component({
  selector: 'app-user-orders-page',
  templateUrl: './user-orders-page.component.html',
  styleUrls: ['./user-orders-page.component.scss']
})
export class UserOrdersPageComponent implements OnInit {
  user$!: Observable<User>;
  orders!: Order[];

  itemHeadings: string[] = ['name', 'amount'];
  discountHeadings: string[] = ['name', 'type'];

  constructor(
    private route: ActivatedRoute,
    private userOrdersService: UserOrdersService
  ) { }

  ngOnInit(): void {
    const userId = Number(this.route.snapshot.paramMap.get('userId'));

    this.user$ = this.userOrdersService.getUser(userId);

    this.userOrdersService.getOrders(userId).pipe(take(1))
    .subscribe((orders: Orders) => this.orders = orders.orders);
  }

  /**
   * Removes a cancelled order from the order list.
   * NB usually I'd prefer retrieving the order list again from the backend,
   * to make 100% sure that the frontend state mirrors the state of the orders in the backend,
   * but since the backend doesn't actually cancel the order I do it like this.
   */
  onOrderCancellation(orderId: number): void {
    this.orders = this.orders.filter((o) => o.id !== orderId);
  }
}
