import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateLoader, TranslateModule, TranslatePipe, TranslateService } from '@ngx-translate/core';
import { createHttpLoaderFactory } from 'src/app/app.module';
import { OrderCardComponent } from '../order-card/order-card.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { UserOrdersPageComponent } from './user-orders-page.component';
import { Order, OrderStatus } from 'src/app/models/order';
import { LocationStrategy } from '@angular/common';
import { MockLocationStrategy } from '@angular/common/testing';

describe('UserOrdersPageComponent', () => {
  let component: UserOrdersPageComponent;
  let fixture: ComponentFixture<UserOrdersPageComponent>;
  const dummyOrders: Order[] = [1, 2, 5, 6, 35]
  .map((id) => new Order().deserialize({ id, ref: 'testref', status: OrderStatus.paid }));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: createHttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [
        UserOrdersPageComponent,
        OrderCardComponent,
        TranslatePipe
      ],
      providers: [
        TranslateService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserOrdersPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.orders = dummyOrders;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should remove orders from the orderlist', () => {
    component.onOrderCancellation(5);

    expect(component.orders.map(order => order.id)).toEqual([1, 2, 6, 35]);
  });

  it('shouldn\'t remove orders from the orderlist if the id passed is not in the order list', () => {
    component.onOrderCancellation(3);

    expect(component.orders.map(order => order.id)).toEqual(dummyOrders.map(order => order.id));
  });
});
