import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { FooterComponent } from './components/footer/footer.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { UserOrdersPageComponent } from './pages/orders/user-orders-page/user-orders-page.component';
import { OrderCardComponent } from './pages/orders/order-card/order-card.component';
import { TableComponent } from './components/table/table.component';
import { DiscountPipe } from './pipes/discount.pipe';
import { SimpleModalModule } from 'ngx-simple-modal';
import { ModalComponent } from './components/modal/modal.component';
import {ToasterModule} from 'angular2-toaster';
import { InterceptorService } from './services/http-interceptor/interceptor.service';

export const createHttpLoaderFactory = (http: HttpClient): TranslateHttpLoader => new TranslateHttpLoader(http, './assets/i18n/', '.json');

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    FooterComponent,
    NavigationComponent,
    UserOrdersPageComponent,
    OrderCardComponent,
    TableComponent,
    DiscountPipe,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: createHttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    SimpleModalModule,
    ToasterModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
