import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth-guard/auth.guard';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { OrderCardComponent } from './pages/orders/order-card/order-card.component';
import { UserOrdersPageComponent } from './pages/orders/user-orders-page/user-orders-page.component';

export const routes: Routes = [
  {
    path: '',
    component: HomePageComponent
  },
  {
    path: 'user/:userId/orders',
    component: UserOrdersPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: '' // if the page doesn't exist, redirect to homepage
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
