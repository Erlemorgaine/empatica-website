import { Deserializable } from './deserializable';

export enum OrderStatus {
    paid = 'paid'
}

export enum ShippingStatus {
    inTransit = 'in_transit'
}

export enum DiscountType {
    percent = 'percent',
    amount = 'amount'
}

export class Tracking implements Deserializable {
    carrier!: string;
    trackingCode!: string;
    status!: ShippingStatus;

    deserialize(input: any): this {
        return Object.assign(this, input);
    }
}

export class Item {
    sku!: string;
    name!: string;
    amount!: number;

    deserialize(input: any): this {
        return Object.assign(this, input);
    }
}
export class Discount {
    name!: string;
    type!: DiscountType;
    value!: number;

    deserialize(input: any): this {
        return Object.assign(this, input);
    }
}

export class Order {
    id!: number;
    ref!: string;
    status!: OrderStatus;
    tracking?: Tracking;
    items: Item[] = [];
    discounts: Discount[] = [];

    deserialize(input: any): this {
        Object.assign(this, input);

        if (!!input.tracking) {
            this.tracking = new Tracking().deserialize(input.tracking);
        }

        this.items = !!input.items ? input.items.map((item: any) => new Item().deserialize(item)) : [];
        this.discounts = !!input.discounts ? input.discounts.map((discount: any) => new Item().deserialize(discount)) : [];

        return this;
    }
}
export class Orders {
    orders: Order[] = [];

    deserialize(input: any): this {
        Object.assign(this, input);

        this.orders = input.orders.map((order: any) => new Order().deserialize(order));

        return this;
    }
}

