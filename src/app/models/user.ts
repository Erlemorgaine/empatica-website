import { Deserializable } from './deserializable';
export class User implements Deserializable {
    id!: number;
    firstName = '';
    lastName = '';
    email!: string;

    deserialize(input: {[k: string]: any}): this {
        return Object.assign(this, input);
    }
}
