# EmpaticaWebsite

This project consists of a home page and a page where the user can view his or her orders.\
The home page appears when the web application is accessed at 'http://localhost:4200/'.

The page with user orders is reachable through '/user/:userId/orders'.

When accessing this page, the application checks in the backend if the user exists by checking it against the data returned by the '/login' endpoint.\
Since this endpoint returns userId 1, the page can in practice only be accessed through '/user/1/orders'.\
Trying to access the page with another userId results in being redirected to the homepage.

To run this project, you need to have npm and the Angular CLI installed.

To install npm, see [here](https://www.npmjs.com/get-npm).\
To install the Angular CLI, see [here](https://cli.angular.io/).

## Browser support

This project has been tested on Chrome 87 & 88, Edge 88, Firefox 84, Safari, Chrome 87 on Android 9, Chrome 87 on iOs 12.4.9, Safari on iOs 12,4,9.

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.\
Run `npm run build:prod` for a production build.

## Test on mobile devices

For testing on mobile devices, go to package.json and replace 0.0.0.0 in the `start:specifyHost` script for your IP address.\
Also, go to environment.ts, and in the backendUrl property replace `'http://localhost:3000'` for `'http://<your-ip-address>:3000'`.\
In the backend, make sure to replace any references to localhost to your IP address.

Then run `npm run start:specifyHost`, and access the site on your mobile device through http://<your-ip-address>:4200.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).\
Make sure that you have Chrome version 88 installed before running the tests!\
Also, make sure that the backend is running when you run de e2e tests, and that the web server is NOT running.

## Running accessibility tests

Install the aXe extension. It should be available for all browsers. [Here](https://chrome.google.com/webstore/detail/axe-web-accessibility-tes/lhdoppojpmngadmnindnejefpokejbdd) is the Chrome extension page.\
Now you should be able to find aXe in the Chrome DevTools menu, and scan the application pages on accessibility issues.
